var $window = $(window);
var $document = $(document);

$window.on('load', function () {

});

$window.on('mousemove', function (e) {

});

$window.on('resize', function () {

});

$document.ready(function () {

});

$window.on('scroll', function () {
});


$(document).on('click ', '.js--go-next-step', goToNextStep);
$(document).on('click ', '.form-step__item--enable', goToPrevStep);
$(document).on('click ', '.btn-form', validateLastStep);


$window.on('touchend', function () {
    return true;
});


function validateFirstStep() {
    var family = $("input[name='family']").val().trim();
    var name = $("input[name='name']").val().trim();
    var lastName = $("input[name='lastName']").val().trim();
    var birthday = $("input[name='birthday']").val().trim();
    var error = 0;
    if(family == 0){
        $("input[name='family']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='family']").closest(".form__item").removeClass("has-error");

    if(name == 0) {
        $("input[name='name']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='name']").closest(".form__item").removeClass("has-error");

    if(lastName == 0) {
        $("input[name='lastName']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='lastName']").closest(".form__item").removeClass("has-error");

    if(birthday == 0) {
        $("input[name='birthday']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='birthday']").closest(".form__item").removeClass("has-error");

    if($('input[name="gender"]:checked').length < 1){
        $(".radio-data").addClass('has-error')
        error++
    } else  $(".radio-data").removeClass('has-error')

    if($('input[name="agreement"]:checked').length < 1){
       $(".check-data").addClass('has-error')
        error++
    } else  $(".check-data").removeClass('has-error')

    return error
}
function validateSecondStep() {
    var phone = $("input[name='phone']").val().trim();
    var email = $("input[name='emailAdress']").val().trim();
    var password = $("input[name='password']").val().trim();
    var passwordRepeat = $("input[name='passwordRepeat']").val().trim();
    var error = 0;

    if(phone == 0){
        $("input[name='phone']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='phone']").closest(".form__item").removeClass("has-error");

    if(email == 0){
        $("input[name='emailAdress']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='emailAdress']").closest(".form__item").removeClass("has-error");

    if(password == 0){
        $("input[name='password']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='password']").closest(".form__item").removeClass("has-error");

    if(passwordRepeat == 0 || passwordRepeat !== password){
        $("input[name='passwordRepeat']").closest(".form__item").addClass("has-error");
        error++
    } else $("input[name='passwordRepeat']").closest(".form__item").removeClass("has-error");

    return error
}
function validateLastStep(e) {
    var key = $("input[name='key']").val().trim();
    var error = 0;

    if(key == 0){
        $("input[name='key']").closest(".form__item").addClass("has-error");
        return false
    } else $("input[name='key']").closest(".form__item").removeClass("has-error");

}


/*
function formValidate() {
    validateFirstStep()
    validateSecondStep()
    validateLastStep()
}
*/
function goToPrevStep() {
    $(".form-data__item").removeClass("form-data__item--active");
    var indexList = $(this).index()
    $(".form-data__item").eq(indexList).addClass("form-data__item--active")
    $(".form-step__item").removeClass("form-step__item--active");
    if(indexList == 0) {
        $(".form-step__item").removeClass("form-step__item--enable");

    }
    $(this).addClass("form-step__item--active");
}
function goToNextStep(e) {
    e.preventDefault();
    var indexList = $(this).closest(".form-data__item").index();
    if(!indexList) {
        var validate = validateFirstStep()
    }
    else{
        var validate = validateSecondStep()
    }

    if(validate === 0){
        $(".form-data__item").removeClass("form-data__item--active");
        $(".form-step__item--active").addClass("form-step__item--enable");
        $(".form-step__item").removeClass("form-step__item--active");
        $(".form-data__item").eq(indexList + 1).addClass("form-data__item--active")
        $(".form-step__item").eq(indexList + 1).addClass("form-step__item--active")
    }


}

